var info, botm, chat, sp1c, sp2c, sp3c, ctd,
    initgot = 0,
    bufft = 0,
    isClsPicOpen = 0,
    isInited = false; //拟态框,大学模块,聊天,倒计时

function initgo() { //异步初始化第二：填入说明信息、渲染主页面

    //爱发电和telegram
    sp1c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><img src="img/afdian.png" alt="爱发电图标" width="50%"/><img src="img/tg.png" alt="telegram图标" width="50%"/></div><div class="card-action"><a href="https://afdian.net/@acted">爱发电</a><a href="https://t.me/joinchat/MUKY6Ej2AvLJEG2JigyE8A">tg群</a></div></div></div>'

    //渲染主页面
    var sp1 = document.getElementById("tab1"),
        sp2 = document.getElementById("tab2"),
        sp3 = document.getElementById("tab3");
    //渲染到DOM
    sp1.innerHTML = '<div class="row">' + sp1c + '</div>'
    sp2.innerHTML = '<div class="row">' + sp2c + '</div>'
    sp3.innerHTML = '<div class="row">' + sp3c + '</div>'
    //初始化标签页们
    let alltab = document.getElementsByClassName("tatatabs");
    let t1 = 0,
        a1 = alltab.length;
    while (t1 <= a1) {
        M.Tabs.init(alltab[t1]);
        t1 += 1;
    }
    document.getElementById("loading").style.display = "none"
    //课程继续
    if (localStorage.getItem("clsup")) {
        dialogAlert("看起来RBQ正在上课呢！");
        clsrestart();
    }
}


var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('DOMContentLoaded', this.onDeviceReady.bind(this), false);
    },
    onDeviceReady: function () {
        //检查数据版本是否兼容,不兼容则执行升级
        if (localStorage.getItem("DataVersion")) {
            if (parseInt(localStorage.getItem("DataVersion")) < 8) {
                dialogAlert("数据版本较低，将执行升级");
                window.location.replace("upgrade/" + localStorage.getItem("DataVersion") + ".html");
            }
        } else {
            localStorage.setItem("DataVersion", 8)
        }


        //初始化标签页
        M.AutoInit();
        info = M.Modal.init(document.getElementById("modal"), {
            onCloseEnd: function () {
                document.getElementById("info_content").innerHTML = '';
                if (!isInited) {//如果没有进行过init，则是远程数据同步的窗口被关闭了。执行init
                    app.initia();
                }
            }
        });
        botm = M.Modal.init(document.getElementById("modalb"), {
            onCloseEnd: function () {
                ClsPicOff();
                document.getElementById("botm_content").innerHTML = '';
            }
        });
        chat = M.Modal.init(document.getElementById("chat"), {});

        if (localStorage.getItem("setting-remote")) {//没怎么做的服务器同步
            //开始联网查询是否有更改
            var xhrst = new XMLHttpRequest()
            xhrst.onreadystatechange = function () {
                if (xhrst.readyState == 4) {
                    if (xhrst.status == 204) { //204代表存储没有发生改变，应直接按照本地存储加载
                        //初始化DOM内容
                        app.initia();
                    } else if (xhrst.status == 200) { //200代表存储发生了改变，且本地客户端应该执行更新
                        document.getElementById("info_header").innerHTML = "远程服务器有更新的存档版本";
                        document.getElementById("info_content").innerHTML = "在远程服务器检测到了您在其他客户端上的存档版本，请您比对后（功能未实现）选择是否导入<br>远端版本：<textarea>" + xhrst.responseText + "</textarea><br>本地版本：<textarea>" + app.dataout() + "</textarea>";
                        document.getElementById("info_footer").innerHTML = "<a  class=\"waves-effect waves-green btn-flat\" href='meowin.html?meowin=" + xhrst.responseText + "'>使用较新版本数据</a><a  class=\"modal-close waves-effect waves-green btn-flat\" href=\"#!\" onclick=\"noupdate('false');\">忽视</a><a  class=\"modal-close waves-effect waves-green btn-flat\" href=\"#!\" onclick=\"noupdate('true');\">要求服务端与本地同步</a>"
                        info.open();
                    } else if (xhrst.status == 403) {
                        dialogAlert("服务端报告您无权访问该服务器，请检查Token。服务器报告信息：" + xhrst.responseText);
                        app.initia();
                    } else {
                        dialogAlert("当前网络不可用，没有更新服务端信息，HTTP代码" + xhrst.status);
                        app.initia();
                    }
                }
            }
            xhrst.open("post", localStorage.getItem("setting-remote") + "/get/");
            let data = new FormData();
            data.append("setting-savetime", localStorage.getItem("setting-savetime"));
            data.append("setting-token", localStorage.getItem("setting-token"));
            data.append("MeowVer", "MeowVer5");
            data.append("force-update", "false");
            data.append("datas", app.dataout());
            xhrst.send(data);
            //设置上传时间戳
            localStorage.setItem("setting-savetime", Math.round(new Date().getTime() / 1000))
        } else {
            //初始化DOM内容
            this.initia();
        }
    },
    dataout: function () {//数据导出模块，MeowVer5
        var t = 0,
            data = {},
            array = ["uni", "cls", "alpha", "slut", "sissy", "couple", "item", "onitem", "buff-oral:", "buff-anal", "buff-water", "buff-enforce", "buff-fuck", "again", "heart", "money", "setting-token", "noval", "buff-U2", "house-adly", "biaozi", "house"];
        while (t < array.length) {
            if (localStorage.getItem(array[t])) {
                data[array[t]] = localStorage.getItem(array[t]);
            }
            t += 1;
        }
        data["ver"] = "MeowVer5"
        return JSON.stringify(data);
    },
    initia: function () { //初始化渲染页面
        if (localStorage.getItem("setting-remote")) {
            //如果设置了远程服务器则在关闭页面前上传最后一次数据
            window.addEventListener('beforeunload', function () {
                // 将最后的喵喵存储事项导出并保存
                let data = new FormData();
                data.append("setting-savetime", localStorage.getItem("setting-savetime"));
                data.append("setting-token", localStorage.getItem("setting-token"));
                data.append("MeowVer", "MeowVer5");
                data.append("force-update", "false");
                data.append("datas", app.dataout());
                navigator.sendBeacon(localStorage.getItem("setting-remote") + "/save/", data);
                //设置上传时间戳
                this.localStorage.setItem("setting-savetime", Math.round(new Date().getTime() / 1000))
                this.console.log("再见");
            });
        }

        //标识曾经进行过init
        isInited = true;
        //初始化，ｓｐ１设置开发版信息
        //sp1c = '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">滑稽信息</span><p>您现在正在使用滚动更新版本，可能会遇上未检测出的bug，主程序版本号Ver2.6.4.4，数据版本Data8.0ADS</p></div><div class="card-action"><a href="https://gitlab.com/acted/h2">来看看源代码</a><a href="https://afdian.net/@acted">爱发电支持下！</a><br><a href="https://t.me/joinchat/MUKY6Ej2AvLJEG2JigyE8A">telegram群</a><a herf="https://acted.gitlab.io/h2-lts">稳定版</a></div></div></div>';
        sp1c = '';
        sp2c = '';
        sp3c = '';
        initgot = 0;
        document.getElementById("loading").style.display = "block";
        //伴侣信息、alpha,sissy,slut,heart，MONEY值
        if (localStorage.getItem("couple")) {
            var couple;
            switch (localStorage.getItem("couple")) {
                case ("0"): {
                    couple = "你的伴侣是♀-莉娅"
                    break;
                }
                case ("1"): {
                    couple = "你的伴侣是⚧-米娅"
                    break;
                }
                default: {
                    couple = "你尚未选择伴侣或存储信息有误"
                }
            }
        }
        if (localStorage.getItem("alpha")) {
            var alpha = localStorage.getItem("alpha");
        } else {
            localStorage.setItem("alpha", 0)
            var alpha = "0"
        }
        if (localStorage.getItem("sissy")) {
            var sissy = localStorage.getItem("sissy");
        } else {
            localStorage.setItem("sissy", 0)
            var sissy = "0"
        }
        if (localStorage.getItem("slut")) {
            var slut = localStorage.getItem("slut");
        } else {
            localStorage.setItem("slut", 0)
            var slut = "0"
        }
        if (localStorage.getItem("heart")) {
            var heart = localStorage.getItem("heart");
        } else {
            localStorage.setItem("heart", 0)
            var heart = "0"
        }
        //开始检查课程文件
        switch (localStorage.getItem("uni")) { //按照章节加载对应代码
            case "0": { //忠贞之夜－第一章
                sp2c += '<div class="col s12 m6 l4"><div class="card-panel hoverable red"><span class="white-text">商店与效果系统在第一章不可用</span></div></div>'
                sp3c += '<div class="col s12 m6 l4"><div class="card-panel hoverable red"><span class="white-text">住所系统在第一章不可用</span></div></div>';
                //检查当前进度，获取课程表信息。
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200) {
                            let json = JSON.parse(xhr.responseText);
                            let code = localStorage.getItem("cls");
                            if (code == undefined) {//如果没有初始化课程，则设为开始课程
                                code = "U0_0";
                                localStorage.setItem("cls", "U0_0");
                            }
                            let clsn = json[code];
                            sp1c += '<div class="col s12 m6 l4"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator" id="test-active-bkg" src="./img/U0/' + code + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + clsn + '<i            class="material-icons right">more_vert</i></span><p><a href="#" onclick="clsup(\'U' + localStorage.getItem("uni") + '/' + code + '\')">进入本卡</a></p></div><div class="card-reveal"><span class="card-title grey-text text-darken-4">' + clsn + '<i class="material-icons right">close</i></span><p>那天之后，好像世界线就此变动了</p><p>第二章：那天之后</p><p>你的alpha值为：' + alpha + '<br>你的slut值为：' + slut + '<br>你的sissy值为：' + sissy + '<br>你与她的关系值为：' + heart + '<br>' + couple + '</p><p>课程代码' + code + '</p><a href="#" onclick="clsup(\'U' + localStorage.getItem("uni") + '/' + code + '\')">进入本卡</a><br><br><a href="#" onclick="ClsPicOn(\'U' + localStorage.getItem("uni") + '/' + code + '\',true)">显示色图</a></div></div></div>';
                            //玩具大小
                            sp1c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">玩具大小</span><p>之前忘了写了，除了在“回家路上”关卡使用中型假阳具以外</p><p>第一章其余部分使用小号阳具</p><p>阳具大小可在wiki上找到</p></div><div class="card-action"><a href="https://gitlab.com/acted/h2/wikis/home">Wiki!!!</a></div></div></div>';
                            initnum(1);
                        } else {
                            dialogAlert("网络错误，代码" + xhr.status);
                        }
                    }
                }
                xhr.open("get", "./json/CLS.json?" + (new Date()).valueOf());
                xhr.send();
                break;
            }
            case "1": { //那天之后－第二章
                sp3c += '<div class="col s12 m6 l4"><div class="card-panel hoverable red"><span class="white-text">住所系统在第二章不可用</span></div></div>';
                //检查当前进度，获取课程表信息。
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200) {
                            let json = JSON.parse(xhr.responseText);
                            let code = localStorage.getItem("cls");
                            if (code == undefined) { //使用了旧版本程序
                                modal("U1error");
                            }
                            let clsn = json[code];
                            sp1c += '<div class="col s12 m6 l4"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator" id="test-active-bkg" src="./img/U' + localStorage.getItem("uni") + '/' + code + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + clsn + '<i            class="material-icons right">more_vert</i></span><p><a href="#" onclick="clsup(\'U' + localStorage.getItem("uni") + '/' + code + '\')">进入本卡</a></p></div><div class="card-reveal"><span class="card-title grey-text text-darken-4">' + clsn + '<i class="material-icons right">close</i></span><p>那天之后，好像世界线就此变动了</p><p>第二章：那天之后</p><p>你的alpha值为：' + alpha + '<br>你的slut值为：' + slut + '<br>你的sissy值为：' + sissy + '<br>你与她的关系值为：' + heart + '<br>' + couple + '</p><p>课程代码' + code + '</p><a href="#" onclick="clsup(\'U' + localStorage.getItem("uni") + '/' + code + '\')">进入本卡</a><br><br><a href="#" onclick="ClsPicOn(\'U' + localStorage.getItem("uni") + '/' + code + '\',true)">显示色图</a></div></div></div>';
                            initnum(2);
                        } else {
                            dialogAlert("网络错误，代码" + xhr.status);
                        }
                    }
                }
                xhr.open("get", "./json/CLS.json?" + (new Date()).valueOf());
                xhr.send();
                //商店加载
                loadshop(2);
                break;
            }
            case "2": { //万圣节派对 
                sp3c += '<div class="col s12 m6 l4"><div class="card-panel hoverable red"><span class="white-text">住所系统在第三章不可用</span></div></div>';
                //检查当前进度，获取课程表信息。
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200) {
                            let json = JSON.parse(xhr.responseText);
                            let code = localStorage.getItem("cls");
                            let clsn = json[code];
                            sp1c += '<div class="col s12 m6 l4"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator" id="test-active-bkg" src="./img/U' + localStorage.getItem("uni") + '/' + code + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + clsn + '<i            class="material-icons right">more_vert</i></span><p><a href="#" onclick="clsup(\'U' + localStorage.getItem("uni") + '/' + code + '\')">进入本卡</a></p></div><div class="card-reveal"><span class="card-title grey-text text-darken-4">' + clsn + '<i class="material-icons right">close</i></span><p>那天之后，好像世界线就此变动了</p><p>第二章：那天之后</p><p>你的alpha值为：' + alpha + '<br>你的slut值为：' + slut + '<br>你的sissy值为：' + sissy + '<br>你与她的关系值为：' + heart + '<br>' + couple + '</p><p>课程代码' + code + '</p><a href="#" onclick="clsup(\'U' + localStorage.getItem("uni") + '/' + code + '\')">进入本卡</a><br><br><a href="#" onclick="ClsPicOn(\'U' + localStorage.getItem("uni") + '/' + code + '\',true)">显示色图</a></div></div></div>';
                            initnum(2);
                        } else {
                            dialogAlert("网络错误，代码" + xhr.status);
                        }
                    }
                }
                xhr.open("get", "./json/CLS.json?" + (new Date()).valueOf());
                xhr.send();
                if (localStorage.getItem("buff-party")) { //如果可以自由选择关卡了
                    sp1c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">第三章关卡选择</span><p>第三章进入派对后就可以自由的选择关卡啦！</p></div><div class="card-action"><a href="#" onclick="clsup(\'U2/smart\')">查看列表</a></div></div></div>';
                }
                loadshop(2);
                break;
            }
            case "3": { //下一步
                //检查当前进度，获取课程表信息。
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200) {
                            let json = JSON.parse(xhr.responseText);
                            let code = localStorage.getItem("cls");
                            let clsn = json[code];
                            sp1c += '<div class="col s12 m6 l4"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator" id="test-active-bkg" src="./img/U' + localStorage.getItem("uni") + '/' + code + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + clsn + '<i            class="material-icons right">more_vert</i></span><p><a href="#" onclick="clsup(\'U' + localStorage.getItem("uni") + '/' + code + '\')">进入本卡</a></p></div><div class="card-reveal"><span class="card-title grey-text text-darken-4">' + clsn + '<i class="material-icons right">close</i></span><p>那天之后，好像世界线就此变动了</p><p>第二章：那天之后</p><p>你的alpha值为：' + alpha + '<br>你的slut值为：' + slut + '<br>你的sissy值为：' + sissy + '<br>你与她的关系值为：' + heart + '<br>' + couple + '</p><p>课程代码' + code + '</p><a href="#" onclick="clsup(\'U' + localStorage.getItem("uni") + '/' + code + '\')">进入本卡</a><br><br><a href="#" onclick="ClsPicOn(\'U' + localStorage.getItem("uni") + '/' + code + '\',true)">显示色图</a></div></div></div>';
                            initnum(2);
                        } else {
                            dialogAlert("网络错误，代码" + xhr.status);
                        }
                    }
                }
                xhr.open("get", "./json/CLS.json?" + (new Date()).valueOf());
                xhr.send();
                loadhouse();//加载住所
                loadshop(2);//加载商店
                break;
            }
            case "4": { //健身房
                //开发模式
                //sp1c += '<div class="col s12 m6 l4"><div class="card-panel hoverable red"><span class="white-text">当前第五章尚未制作完成，没有图片的关卡代表没有制作。</span></div></div>';
                //

                //检查当前进度，获取课程表信息。
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200) {
                            let json = JSON.parse(xhr.responseText);
                            let code = localStorage.getItem("cls");
                            let clsn = json[code];
                            sp1c += '<div class="col s12 m6 l4"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator" id="test-active-bkg" src="./img/U' + localStorage.getItem("uni") + '/' + code + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + clsn + '<i            class="material-icons right">more_vert</i></span><p><a href="#" onclick="clsup(\'U4/' + code + '\')">进入本卡</a></p></div><div class="card-reveal"><span class="card-title grey-text text-darken-4">' + clsn + '<i class="material-icons right">close</i></span><p>那天之后，好像世界线就此变动了</p><p>第四章：下一步</p><p>你的alpha值为：' + alpha + '<br>你的slut值为：' + slut + '<br>你的sissy值为：' + sissy + '<br>你与她的关系值为：' + heart + '</p><p>课程代码' + code + '</p><a href="#" onclick="clsup(\'U4/' + code + '\')">进入本卡</a><br><br><a href="#" onclick="ClsPicOn(\'U' + localStorage.getItem("uni") + '/' + code + '\',true)">显示色图</a></div></div></div>';
                            initnum(2);
                        } else {
                            dialogAlert("网络错误，代码" + xhr.status);
                        }
                    }
                }
                xhr.open("get", "./json/CLS.json?" + (new Date()).valueOf());
                xhr.send();
                loadhouse();//加载住所
                loadshop(2);//加载商店，正常运转的时候应该是2
                //initgo();
                break;
            }
            case "5": { //假日
                //开发模式
                sp1c += '<div class="col s12 m6 l4"><div class="card-panel hoverable red"><span class="white-text">当前第六章尚未制作</span></div></div>';
                break;

                //检查当前进度，获取课程表信息。
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200) {
                            let json = JSON.parse(xhr.responseText);
                            let code = localStorage.getItem("cls");
                            let clsn = json[code];
                            sp1c += '<div class="col s12 m6 l4"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator" id="test-active-bkg" src="./img/U' + localStorage.getItem("uni") + '/' + code + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + clsn + '<i            class="material-icons right">more_vert</i></span><p><a href="#" onclick="clsup(\'U4/' + code + '\')">进入本卡</a></p></div><div class="card-reveal"><span class="card-title grey-text text-darken-4">' + clsn + '<i class="material-icons right">close</i></span><p>那天之后，好像世界线就此变动了</p><p>第四章：下一步</p><p>你的alpha值为：' + alpha + '<br>你的slut值为：' + slut + '<br>你的sissy值为：' + sissy + '<br>你与她的关系值为：' + heart + '</p><p>课程代码' + code + '</p><a href="#" onclick="clsup(\'U4/' + code + '\')">进入本卡</a><br><br><a href="#" onclick="ClsPicOn(\'U' + localStorage.getItem("uni") + '/' + code + '\',true)">显示色图</a></div></div></div>';
                            initnum(2);
                        } else {
                            dialogAlert("网络错误，代码" + xhr.status);
                        }
                    }
                }
                xhr.open("get", "./json/CLS.json?" + (new Date()).valueOf());
                xhr.send();
                loadhouse();//加载住所
                loadshop(2);//加载商店，正常运转的时候应该是2
                //initgo();
                break;
            }
            case "gameover": { //游戏结束（异常）
                sp1c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">结束信息</span><p>GameOver！你完蛋啦！</p></div><div class="card-action"><a href="#" onclick="clean()">重置数据以重新开始</a></div></div></div>';
                sp2c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">结束信息</span><p>GameOver！你完蛋啦！</p></div><div class="card-action"><a href="#" onclick="clean()">重置数据以重新开始</a></div></div></div>';
                sp3c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">结束信息</span><p>GameOver！你完蛋啦！</p></div><div class="card-action"><a href="#" onclick="clean()">重置数据以重新开始</a></div></div></div>';
                initgo();
                break;
            }
            default: { //啥也没有
                sp1c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">开始游戏</span><p>你还没开始游戏呢！</p></div><div class="card-action"><a href="#" onclick="uni(0)">开始游戏</a></div></div></div>';
                sp2c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">开始游戏</span><p>你还没开始游戏呢！</p></div><div class="card-action"><a href="#" onclick="uni(0)">开始游戏</a></div></div></div>';
                sp3c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">开始游戏</span><p>你还没开始游戏呢！</p></div><div class="card-action"><a href="#" onclick="uni(0)">开始游戏</a></div></div></div>';
                initgo();
                break;
            }
        }


        function loadhouse() {
            //开发模式
            sp3c += '<div class="col s12 m6 l4"><div class="card-panel hoverable red"><span class="white-text">当前住所为开发模式，尚未开发完成。</span></div></div>';
            if (!localStorage.getItem("house")) {
                sp3c += '<div class="col s12 m6 l4"><div class="card-panel hoverable red"><span class="white-text">住所系统暂不可用</span></div></div>';
            } else {
                //客厅
                sp3c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">客厅</span><p>哇，好大的客厅！</p></div><div class="card-action"><a href="#" onclick="clsup(\'house/living\')">查看列表</a></div></div></div>';
                //门厅
                sp3c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">门厅</span><p>简单的全身镜和门，总感觉会发生什么故事</p></div><div class="card-action"><a href="#" onclick="clsup(\'house/door\')">查看列表</a></div></div></div>';
                //卧室
                sp3c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">卧室</span><p>卧室，各种小故事的发源地呢</p></div><div class="card-action"><a href="#" onclick="clsup(\'house/bedroom\')">查看列表</a></div></div></div>';
                //阳台
                sp3c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">阳台</span><p>春光明媚，适合发情的地方！</p></div><div class="card-action"><a href="#" onclick="clsup(\'house/balcony\')">查看列表</a></div></div></div>';
                //性爱地牢
                sp3c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">性爱地牢</span><p>每个绒布球家都必备的地方。</p></div><div class="card-action"><a href="#" onclick="clsup(\'house/lovejail\')">查看列表</a></div></div></div>';
                //工作室
                sp3c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">工作室</span><p>该干活啦！鸽子们</p></div><div class="card-action"><a href="#" onclick="clsup(\'house/work\')">查看列表</a></div></div></div>';
            }
        }


        function loadshop(go) { //商店加载
            if (localStorage.getItem("money")) { //只在有钱的时候显示购买物品选项
                sp2c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">商店</span><p>这里显示的是你当前拥有的物品</p><p>你可在此购买更多商品，你当前拥有金钱' + localStorage.getItem("money") + '￥</p></div><div class="card-action"><a href="#" onclick="modal(\'shop_clothes\')">进入衣服商店</a><a href="#" onclick="modal(\'shop_toy\')">进入玩具商店</a></div></div></div>';
            } else {
                sp2c += '<div class="col s12 m6 l4"><div class="card hoverable"><div class="card-content"><span class="card-title">商店</span><p>这里显示的是你当前拥有的物品</p><p>你可在此购买更多商品，但你当前没有钱</p></div></div></div>';
            }
            var xhr2 = new XMLHttpRequest();
            xhr2.onreadystatechange = function () { //要求网络请求ＩＴＥＭ信息
                if (xhr2.readyState == 4) {
                    if (xhr2.status == 200) {
                        var json = JSON.parse(xhr2.responseText);
                        if (!localStorage.getItem("item")) { //如果根本没有ｉｔｅｍ则直接跳过
                            initnum(parseInt(go));
                        } else {
                            var item = localStorage.getItem("item").split("/meow/");
                            var onitem = localStorage.getItem("onitem");
                            var t = 0;
                            if (onitem != null) { //检测当前激活物品是否为空，不是的话开始增加为图片设置颜色
                                while (t < item.length) {
                                    if (onitem.indexOf(item[t]) > -1) { //检测是否已经激活
                                        if (json[item[t]]["canoff"] == "no") { //检测是否允许暂停
                                            sp2c += '<div class="col s12 m6 l4"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator" src="./img/' + item[t] + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + json[item[t]]["name"] + '<i                    class="material-icons right">more_vert</i></span></div><div class="card-reveal"> <span class=\"card-title grey-text text-darken-4\">' + json[item[t]]["name"] + '<i class=\"material-icons right\">close</i></span>' + json[item[t]]["reveal"] + '<br><a href="#" onclick="itemremove(\'' + item[t] + '\')">移除本物品</a></div></div></div>'
                                        } else {
                                            sp2c += '<div class="col s12 m6 l4"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator" src="./img/' + item[t] + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + json[item[t]]["name"] + '<i                    class="material-icons right">more_vert</i></span><p><a href="#" onclick="itemoff(\'' + item[t] + '\')">存档此物品</a></p></div><div class="card-reveal"> <span class=\"card-title grey-text text-darken-4\">' + json[item[t]]["name"] + '<i class=\"material-icons right\">close</i></span>' + json[item[t]]["reveal"] + '<a href="#" onclick="itemoff(\'' + item[t] + '\')">存档本物品</a><br><a href="#" onclick="itemremove(\'' + item[t] + '\')">移除本物品</a></div></div></div>'
                                        }

                                    } else {
                                        sp2c += '<div class="col s12 m6 l4"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator grayimg" src="./img/' + item[t] + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + json[item[t]]["name"] + '<i                    class="material-icons right">more_vert</i></span><p><a href="#" onclick="itemon(\'' + item[t] + '\')">激活此物品</a></p></div><div class="card-reveal">           <span class=\"card-title grey-text text-darken-4\">' + json[item[t]]["name"] + '<i class=\"material-icons right\">close</i></span>' + json[item[t]]["reveal"] + '<a href="#" onclick="itemon(\'' + item[t] + '\')">激活本物品</a><br><a href="#" onclick="itemremove(\'' + item[t] + '\')">移除本物品</a></div></div></div>'
                                    }
                                    t += 1;
                                    console.log("商店图文加载" + t);
                                }
                            } else {
                                while (t < item.length) {
                                    sp2c += '<div class="col s12 m6 l4"><div class="card small hoverable"><div class="card-image waves-effect waves-block waves-light"><img class="activator grayimg" src="./img/' + item[t] + '.jpg"></div><div class="card-content"><span class="card-title activator grey-text text-darken-4">' + json[item[t]]["name"] + '<i                    class="material-icons right">more_vert</i></span><p><a href="#" onclick="itemon(\'' + item[t] + '\')">激活此物品</a></p></div><div class="card-reveal">           <span class=\"card-title grey-text text-darken-4\">' + json[item[t]]["name"] + '<i class=\"material-icons right\">close</i></span>' + json[item[t]]["reveal"] + '<a href="#" onclick="itemon(\'' + item[t] + '\')">激活本物品</a><br><a href="#" onclick="itemremove(\'' + item[t] + '\')">移除本物品</a></div></div></div>'
                                    t += 1;
                                    console.log("商店图文加载" + t);
                                }
                            }
                            initnum(parseInt(go));
                        }
                    } else {
                        dialogAlert("网络错误，代码" + xhr.status);
                    }
                }
            }
            xhr2.open("get", "./json/ITEM.json?" + (new Date()).valueOf());
            xhr2.send();
        }

        function initnum(num) { //初始化异步检测，等待所以异步加载均完成后渲染主页面
            initgot += 1;
            console.log("主页面异步加载" + initgot);
            if (initgot >= num) {
                initgo();
                initgot = 0;
            }
        }
    }
};

function noupdate(force) {//得到服务器的新版数据后，决定不更改
    app.initia();
    var xhr = new XMLHttpRequest();
    xhr.open("post", localStorage.getItem("setting-remote") + "/save/");
    let data = new FormData();
    data.append("setting-savetime", localStorage.getItem("setting-savetime"));
    data.append("setting-token", localStorage.getItem("setting-token"));
    data.append("MeowVer", "MeowVer5");
    data.append("force-update", force);
    data.append("datas", app.dataout());
    xhr.send(data);
}

app.initialize();
