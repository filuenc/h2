//没做完
export function clsup_e() {
    clsshow(
        "发型",
        '<p>"当我的生活有了变化时，我一定会换个新发型！我觉得如果你换个新发型，一定会变得更可爱的。来，我认识一个很棒的造型师，他的店就在拐角那。“，米娅很开心的说着。</p><p>和现在一样（alpha+2）<br>女式短发（sissy+1）<br>染色的女式短发（sissy+2）<br>接发（slut+1）<br>接法并染发（slut+1，sissy+1）</p>',
        0,
        "U1/U1_5",
        //'<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'same\')">和现在一样</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'short\')">女士短发</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'short_color\')">染色的女士短发</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'long\')">接发</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'long_color\')">染色的接发</a>'
        [{ "btnfun": "clsfin(\'U1/U1_5\',\'same\')", "btnname": "和现在一样" }, { "btnfun": "clsfin(\'U1/U1_5\',\'short\')", "btnname": "女士短发" }, { "btnfun": "clsfin(\'U1/U1_5\',\'short_color\')", "btnname": "染色的女士短发" }, { "btnfun": "clsfin(\'U1/U1_5\',\'long\')", "btnname": "接发" }, { "btnfun": "clsfin(\'U1/U1_5\',\'short\')", "btnname": "染色的接发" }]
    );
}
export function clsfin_e(info) {
    switch (info) {
        case "same": {//不变
            alpha(2);
            localStorage.setItem("cls", "U1_7");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        case "short": {//短发
            sissy(1);
            clsshow("发型",
                '<p>去淘宝买一个短的黑色假发</p>',
                0,
                "U1/U1_5",
                //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'e\')">完成</a>'
                [{"btnfun":"clsfin(\'U1/U1_5\',\'e\')","btnname":"完成"}]
            )
            break;
        }
        case "short_color": {//短发染
            sissy(2);
            clsshow("发型",
                '<p>去淘宝买一个短的除黑色外的假发</p>',
                0,
                "U1/U1_5",
                //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'e\')">完成</a>'
                [{"btnfun":"clsfin(\'U1/U1_5\',\'e\')","btnname":"完成"}]
            )
            break;
        }
        case "long": {//接发
            slut(1);
            clsshow("发型",
                '<p>去淘宝买一个黑色长假发</p>',
                0,
                "U1/U1_5",
                //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'e\')">完成</a>'
                [{"btnfun":"clsfin(\'U1/U1_5\',\'e\')","btnname":"完成"}]
            )
            break;
        }
        case "long": {//接发+染色
            slut(1);
            sissy(1);
            clsshow("发型",
                '<p>去淘宝买一个除黑色外的长假发</p>',
                0,
                "U1/U1_5",
                //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'e\')">完成</a>'
                [{"btnfun":"clsfin(\'U1/U1_5\',\'e\')","btnname":"完成"}]
            )
            break;
        }
        case "e": {
            localStorage.setItem("cls", "U1_7");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题")
        }
    }
}


/*
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'same\')">和现在一样</a>
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'short\')">女士短发</a>
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'short_color\')">染色的女士短发</a>
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'long\')">接发</a>
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_5\',\'long_color\')">染色的接发</a>
*/