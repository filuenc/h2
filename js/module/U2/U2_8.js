export function clsup_e() {
    clsshow(
        "时间到",
        '<p>当锁咔嚓一声锁上后，你终于明白了事态的严重性。但是还没有结束。她从盒子里拿了些绳子出来，想了一会。</p><p>“现在我要把你捆起来丢到角落里让你跪着反思去。对了，今晚有个派对，顺便想想穿什么去。我们一起去，把这作为你赎罪的开始吧。”</p><p>-1,2-双腿屈膝开腿缚<br>-3，4-双腿屈膝开脚缚+身体束缚<br>-5，6-模仿左图</p><p>[可选择投掷骰子]</p>',
        0,
        "U2/U2_8",
        //'<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_8\',\'out\')">双腿屈膝开脚缚</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_8\',\'out+band\')">双腿屈膝开脚缚+身体束缚</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_8\',\'left\')">模仿左图</a><a href="#!" class="waves-effect waves-green btn-flat" onclick="random()">使用骰子</a>'
        [{"btnfun":"clsfin(\'U2/U2_8\',\'out\')","btnname":"双腿屈膝开脚缚"},{"btnfun":"clsfin(\'U2/U2_8\',\'out+band\')","btnname":"双腿屈膝开脚缚+身体束缚"},{"btnfun":"clsfin(\'U2/U2_8\',\'left\')","btnname":"模仿左图"},{"btnfun":"random()","btnname":"使用骰子","noclose":true}]
    );
}
export function clsfin_e(info) {
    switch (info) {
        case "out": { //双腿屈膝开脚缚
            clsshow("时间到",
                '<p>双腿的大腿小腿捆在一起，呈鸭子坐形态。维持1小时</p>',
                3600,
                "U2/U2_8",
                //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_8\',\'e\')">完成</a>'
                [{"btnfun":"clsfin(\'U2/U2_8\',\'e\')","btnname":"完成"}]
            )
            break;
        }
        case "out+band": { //+身体束缚
            clsshow("时间到",
                '<p>双腿的大腿小腿捆在一起，呈鸭子坐形态。双手锁在双脚上。维持1小时</p>',
                3600,
                "U2/U2_8",
                //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_8\',\'e\')">完成</a>'
                [{"btnfun":"clsfin(\'U2/U2_8\',\'e\')","btnname":"完成"}]
            )
            break;
        }
        case "left": { //模仿左图
            clsshow("时间到",
                '<p>我们暂时也没啥教程，自己努力吧！<a href="img/U2/U2_8.jpg" target="_blank">看图片</a>维持1小时</p>',
                3600,
                "U2/U2_8",
                //'  <a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_8\',\'e\')">完成</a>'
                [{"btnfun":"clsfin(\'U2/U2_8\',\'e\')","btnname":"完成"}]
            )
            break;
        }
        case "e": {
            localStorage.setItem("cls", "U2_3");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题");
            break;
        }
    }
}


/*
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_7\',\'keep\')">保持不变</a>
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_7\',\'do\')">挣扎一下下</a>
<a href="#!" class="disabled timeset modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_7\',\'e\')">完成</a>
*/