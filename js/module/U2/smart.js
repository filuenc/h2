//用于在第三章弹出可以跳转到哪个章节
export function clsup_e() {
    if (localStorage.getItem("clsup")) { //检查是否有课程继续
        dialogAlert("RBQ需要先上完现在的课程呢！");
        clsrestart()
        return;
    }
    if (localStorage.getItem("buff-U2")) {//如果有可重复关卡
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {//获取JSON来得知其关卡名称
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var json = JSON.parse(xhr.responseText);
                    var list = localStorage.getItem("buff-U2").split("/meow/"), html = '<div class="collection">';
                    var t = 0, a = list.length;
                    while (t < a) {
                        html += '<a href="#!" class="collection-item" onclick="clsfin(\'U2/smart\',\'' + list[t] + '\')">'+json[list[t]]+'</a>'
                        t += 1;
                    }
                    html += '</div>'
                    document.getElementById("info_header").innerHTML = "第三章关卡重复";
                    document.getElementById("info_content").innerHTML = html;
                    document.getElementById("info_footer").innerHTML = "<a  class=\"modal-close waves-effect waves-green btn-flat\" href=\"#!\">关闭</a>";
                    info.open();

                } else {
                    dialogAlert("网络错误，代码" + xhr.status)
                }
            }
        }
        xhr.open("get", "json/CLS.json?"+(new Date()).valueOf());
        xhr.send();
    }else{
        dialogAlert("当前无可重复关卡")
    }
}
export function clsfin_e(code) {
    info.close();
    localStorage.setItem("cls",code);
    app.initia();
}


/*
 <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U1/U1_0_0\',\'0\')">继续</a>
*/