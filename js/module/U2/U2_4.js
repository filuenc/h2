export  function clsup_e(){
    clsshow(
        "扇耳光",
        "<p>她向你怒吼的同时举起了她的胳膊。</p><p>“你怎么能这样！我们才在一起几天，你怎么能这么混蛋！！！你背叛了我去和一堆陌生人搞在一起，为什么？你那么想要鸡吧吗？我还不能满足你吗？我给你买的玩具还不能满足你吗？”<?p><p>她最后狠狠地扇了你一巴掌</p><p>“你会对你的所作所为后悔的！</p><br><p>1,2,3-扇你的脸</p><p>4,5,6-扇你两边脸</p><p>[可选择投掷骰子]</p>",
        0,
        "U2/U2_4",
        //' <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_4\',\'single\')">单侧</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_4\',\'double\')">双侧</a><a href="#!" class="waves-effect waves-green btn-flat" onclick="random()">使用骰子</a>'
        [{"btnfun":"clsfin(\'U2/U2_4\',\'single\')","btnname":"单侧"},{"btnfun":"clsfin(\'U2/U2_4\',\'double\')","btnname":"双侧"},{"btnfun":"random()","btnname":"使用骰子","noclose":true}]
    )
 }

 export function clsfin_e(info){
     switch(info){
         case "single":{
             clsshow(
                 "扇耳光",
                 "<p>尽可能狠地扇你的一边脸，应当在脸上留下手印</p>",
                 30,
                 "U2/U2_4",
                 //'<a href="#!" class="modal-close waves-effect waves-green btn-flat disabled timeset" onclick="clsfin(\'U2/U2_4\',\'done\')">完成</a>'
                 [{"btnfun":"clsfin(\'U2/U2_4\',\'done\')","btnname":"完成"}]
             )
             break;
         }
         case "double":{
            clsshow(
                "扇耳光",
                "<p>将你两侧脸扇红</p>",
                30,
                "U2/U2_4",
                //'<a href="#!" class="modal-close waves-effect waves-green btn-flat disabled timeset" onclick="clsfin(\'U2/U2_4\',\'done\')">完成</a>'
                [{"btnfun":"clsfin(\'U2/U2_4\',\'done\')","btnname":"完成"}]
            )
            break;
        }
        case "done":{
            localStorage.setItem("cls", "U2_6");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
     }
 }