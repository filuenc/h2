export function clsup_e() {
    clsshow(
        "湿漉漉的事故",
        '<p>你走进浴室想梳洗一下，但当你打开水龙头时，龙头突然坏掉了，水喷了出来，把你全身都完全淋湿了。</p><p>外面的人冲了进来，关掉了阀门。</p><p>你完全湿透了，也许你可以因此赢得湿T恤小姐比赛</p><p>也许不能，你还没有柰子</p><p>进入淋浴间把自己全身淋湿，如果穿着了猫娘套装，则可以脱掉除耳朵和尾巴的衣服</p><p>请选择下一步进入哪个关卡</p><p>部分关卡具有可重复性，不过，你的进度将回到该关卡。</p><p>你可以在帮助信息中找到关卡解释图</p>',
        0,
        "U2/U2_21",
        //' <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_21\',\'0\')">前往“入场费”关卡</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_21\',\'1\')">前往“马桶play”关卡</a>'
        [{"btnfun":"clsfin(\'U2/U2_21\',\'0\')","btnname":"前往“入场费”关卡"},{"btnname":"前往“马桶play”关卡","btnfun":"clsfin(\'U2/U2_21\',\'1\')"}]
    );
}

export function clsfin_e(info) {
    switch(info){
        case "0":{
            localStorage.setItem("cls", "U2_20");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        case "1":{
            localStorage.setItem("cls", "U2_23");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题");
            break;
        }
    }
}