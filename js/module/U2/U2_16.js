export function clsup_e() {
    clsshow(
        "洗劫冰箱",
        "<p>似乎有一大群人挤在厨房里，进行着某种竞赛。<br>把各种食物从冰箱里拿出了，赛到菊花里。</p><br><p>规则：可以用润滑液，每插入一样物品等于该物品价格。蔬菜和水果每扩展1CM可多获得$1，罐装食品必须把里面的东西灌入菊穴，再把空罐头插进去。</p><br><p>大于6CM时，可以额外获得$10（仅一次）</p><p>精灵套装可将所有的钱数x2</p>",
        0,
        "U2/U2_16",
        //'  <a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'1\')">增加1块钱</a><a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'5\')">增加5块钱</a><a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'10\')">增加10块钱</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'done\')">完成</a>'
        [{"btnfun":"clsfin(\'U2/U2_16\',\'1\')","btnname":"增加1块钱","noclose":true},{"btnfun":"clsfin(\'U2/U2_16\',\'5\')","btnname":"增加5块钱","noclose":true}
        ,{"btnfun":"clsfin(\'U2/U2_16\',\'10\')","btnname":"增加10块钱","noclose":true},{"btnfun":"clsfin(\'U2/U2_16\',\'done\')","btnname":"完成"}]
    );
}
export function clsfin_e(info) {
    switch (info){
        case "1":{
            money(1);
            dialogAlert("成功增加1块钱","成功")
            break;
        }
        case "5":{
            money(5);
            dialogAlert("成功增加5块钱","成功")
            break;
        }
        case "10":{
            money(10);
            dialogAlert("成功增加10块钱","成功")
            break;
        }
        case "done": {
            if(localStorage.getItem("buff-U2")){
                if(localStorage.getItem("buff-U2").indexOf("U2_16") == -1){
                        localStorage.setItem("buff-U2",localStorage.getItem("buff-U2")+"/meow/U2_16");
                }
            }else{
                localStorage.setItem("buff-U2", "U2_16");
            }
            clsshow(
                "洗劫冰箱",
                "<p>请选择下一步进入哪个关卡</p><p>部分关卡具有可重复性，不过，你的进度将回到该关卡。</p><p>你可以在帮助信息中找到关卡解释图</p>",
                0,
                "U2/U2_16",
                //' <a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'drink\')">前往“想喝什么吗？”关卡</a><a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'water\')">前往“灌肠锦标赛”关卡</a>'
                [{"btnfun":"clsfin(\'U2/U2_16\',\'drink\')","btnname":"前往“想喝什么吗？”关卡"},{"btnfun":"clsfin(\'U2/U2_16\',\'water\')","btnname":"前往“灌肠锦标赛”关卡"}]
            );
            break;
        }
        case "drink":{
            localStorage.setItem("cls", "U2_14");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        case "water":{
            localStorage.setItem("cls", "U2_17");
            /*dialogAlert("本关卡完成", "成功");*/
            ClsPicOff();
            app.initia();
            break;
        }
        default: {
            dialogAlert("系统错误，请点击课程卡片获取课程编码并向群里报告此问题");
            break;
        }
    }
}


/* 
 <a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'1\')">增加1块钱</a>
   <a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'5\')">增加5块钱</a>
  <a href="#!" class="waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'10\')">增加10块钱</a>
<a href="#!" class="modal-close waves-effect waves-green btn-flat" onclick="clsfin(\'U2/U2_16\',\'done\')">完成</a>
*/