//第三章暗精灵
export function buff_e(){
    if(localStorage.getItem("uni")=="2"){
        oral();
    }
    return;
}
function oral(){
    if(!localStorage.getItem("buff-oral")){
        var data={
            timeset:5,
            bpm:0,
            word:"",
            sissy:0,
            slut:0,
            alpha:0,
            moneyin:0,
            sizer:1,
            sizel:2
        }
    }else{
        try{
            var data=JSON.parse(localStorage.getItem("buff-oral"));
            data.sizer=parseInt(data.sizer)+1;
            data.sizel=parseInt(data.sizel)+2;
            data.timeset=parseInt(data.timeset)+5;

        }
        catch (err){
            dialogAlert("buff数据错误，已重置");
            localStorage.removeItem("buff-oral");
            oral();
            return;
        }
    }
    localStorage.setItem("buff-oral",JSON.stringify(data));
}

export function add_e(){
    slut(3);
    sissy(2)
    itemadd2("U2sprit_light");
    setTimeout(() => {
        itemon("U2sprit_light");
    }, 100);
}
export function remove_e(){
    if(localStorage.getItem("uni")=="2"){
        dialogAlert("第三章关键道具不允许移除")
    }else{
    itemremove3("U2sprit_light");
    }
}
